---
layout: page
title: About
permalink: /about/
---

InfoSec junkie, hacker, aspiring Penetration Tester/Red Team member

Making the world a better place, fuzzing one thing at a time

OSCP journey in progress

[HackTheBox](https://www.hackthebox.eu/profile/76302)